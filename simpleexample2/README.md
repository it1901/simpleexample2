# Simpleexample2

Dette prosjektet er et eksempel på en nokså minimalistisk trelagsapplikasjon, altså med et domenelag, brukergrensesnitt (UI) og persistens (lagring).
Prosjektet inneholder tester for alle lagene, med rimelig god dekningsgrad.

Prosjektet er konfigurert som et multi-modul-prosjekt med maven, hvor domene- og persistenslagene ligger i en [kjernemodul (core)](core/README.md) og brukergrensesnittet i en [ui-modul (fxui)](fxui/README.md).

## Bygging med maven

I et multi-modul-prosjekt så inneholder rot-prosjektet sin **pom.xml** en oversikt over del-prosjektene (sub-modulene) og konfigurasjonen som er felles for disse.
Det som vi har lagt inn av felles konfigurasjon (så langt) er plugin-versjoner- og innstillinger og en del avhengigheter.

## Arkitekturdiagram

![Arkitektur](architecture.png)
