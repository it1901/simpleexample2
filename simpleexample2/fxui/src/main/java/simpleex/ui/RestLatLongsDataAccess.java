package simpleex.ui;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.util.Collection;
import java.util.Collections;
import simpleex.core.LatLong;
import simpleex.core.LatLongs;

/*
 * @startuml class FxAppController class LatLongs class BorderPane class "ListView<LatLong>" as
 * ListView class "fxmapcontrol.MapBase" as MapBase
 * 
 * BorderPane *--> ListView: "left" BorderPane *--> MapBase: "center"
 * 
 * FxAppController --> LatLongs: "latLongs" FxAppController --> MapBase: "mapView" FxAppController
 * --> ListView: "locationListView"
 * 
 * @enduml
 */

/**
 * Data access object using rest.
 */
public class RestLatLongsDataAccess implements LatLongsDataAccess {

  private final String baseUrlString;

  private final ObjectMapper objectMapper;

  public RestLatLongsDataAccess(final String baseUrlString, final ObjectMapper objectMapper) {
    this.baseUrlString = baseUrlString;
    this.objectMapper = objectMapper;
  }

  protected ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  private URI getRequestUri(final String path) {
    try {
      return new URI(baseUrlString + path);
    } catch (final URISyntaxException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @Override
  public Collection<LatLong> getAllLatLongs() {
    final URI requestUri = getRequestUri("");
    final HttpRequest request = HttpRequest.newBuilder(requestUri)
        .header("Accept", "application/json").GET().build();
    try {
      final HttpResponse<String> response =
          HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
      final String responseString = response.body();
      final LatLongs readValue = getObjectMapper().readValue(responseString, LatLongs.class);
      return readValue.toList();
    } catch (JsonParseException | JsonMappingException e) {
      System.err.println(e.toString());
    } catch (IOException | InterruptedException e) {
      System.err.println(e.toString());
    }
    return Collections.emptyList();
  }

  @Override
  public LatLong getLatLong(final int num) {
    final HttpRequest request =
        HttpRequest.newBuilder(getRequestUri("/" + num))
          .header("Accept", "application/json").GET().build();
    try {
      final HttpResponse<InputStream> response =
          HttpClient.newBuilder().build().send(
            request, HttpResponse.BodyHandlers.ofInputStream()
          );
      return getObjectMapper().readValue(response.body(), LatLong.class);
    } catch (JsonParseException | JsonMappingException e) {
      System.err.println(e.toString());
    } catch (IOException | InterruptedException e) {
      System.err.println(e.toString());
    }
    return null;
  }

  @Override
  public void setLatLong(final int index, final LatLong latLong) {
    try {
      final HttpRequest request = HttpRequest.newBuilder(getRequestUri("/" + index))
          .header("Content-Type", "application/json")
          .header("Accept", "application/json")
          .PUT(BodyPublishers.ofString(getObjectMapper().writeValueAsString(latLong)))
          .build();
      final HttpResponse<InputStream> response =
          HttpClient.newBuilder().build().send(
            request, HttpResponse.BodyHandlers.ofInputStream()
          );
      final int realIndex = getObjectMapper().readValue(response.body(), Integer.class);
      if (realIndex < 0) {
        throw new IndexOutOfBoundsException(realIndex);
      }
    } catch (final JsonProcessingException e) {
      throw new RuntimeException(e);
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public int addLatLong(final LatLong latLong) {
    try {
      final LatLongs latLongs = new LatLongs(latLong);
      final HttpRequest request = HttpRequest.newBuilder(getRequestUri(""))
          .header("Content-Type", "application/json")
          .header("Accept", "application/json")
          .POST(BodyPublishers.ofString(getObjectMapper().writeValueAsString(latLongs)))
          .build();
      final HttpResponse<InputStream> response =
          HttpClient.newBuilder()
          .build()
          .send(request, HttpResponse.BodyHandlers.ofInputStream());
      final int realIndex = getObjectMapper().readValue(response.body(), Integer.class);
      if (realIndex < 0) {
        throw new IndexOutOfBoundsException(realIndex);
      }
      return realIndex;
    } catch (final JsonProcessingException e) {
      throw new RuntimeException(e);
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
