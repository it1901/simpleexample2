package simpleex.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class LatLongTest {

  @Test
  public void testToString() {
    final LatLong latLong = new LatLong(63.0, 10.0);
    assertEquals(Double.toString(63) + "," + Double.toString(10), latLong.toString());
  }

  @Test
  public void testValueOf() {
    testLatLong(LatLong.valueOf("63.0, 10.0"), 63.0, 10.0);
    testLatLong(LatLong.valueOf("63.0, 10.0", ","), 63.0, 10.0);
    testLatLong(LatLong.valueOf("63.0; 10.0", ";"), 63.0, 10.0);
    try {
      testLatLong(LatLong.valueOf("63.0; 10.0", ","), 63.0, 10.0);
      fail("Should throw IllegalArgumentException");
    } catch (final IllegalArgumentException e) {
      // ok
    } catch (final Exception e) {
      fail("Should throw IllegalArgumentException");
    }
  }

  private void testLatLong(final LatLong latLong, final double lat, final double lon) {
    assertEquals(lat, latLong.getLatitude(), 0.0);
    assertEquals(lon, latLong.getLongitude(), 0.0);
  }

  @Test
  public void testEquals() {
    assertTrue(new LatLong(63.0, 10.0).equals(new LatLong(63.0, 10.0)));
    assertFalse(new LatLong(10.0, 63.0).equals(new LatLong(63.0, 10.0)));
    assertFalse(new LatLong(10.0, 63.0).equals(null));
  }

  @Test
  public void testHashCode() {
    final Map<LatLong, String> map = new HashMap<>();
    map.put(new LatLong(63.0, 10.0), "first");
    map.put(new LatLong(63.0, 10.0), "second");
    assertEquals(1, map.size());
    assertEquals("second", map.get(new LatLong(63.0, 10.0)));
  }

  @Test
  public void testDistance() {
    final LatLong trd = new LatLong(63.4217137055, 10.4221522734);
    final LatLong str = new LatLong(63.0339713594, 10.2946225585);

    checkDistance(trd.distance(trd), 0.0, 10.0);
    checkDistance(str.distance(str), 0.0, 10.0);
    checkDistance(trd.distance(str), 43000.0, 45000.0);
    checkDistance(str.distance(trd), 43000.0, 45000.0);

    checkDistance(LatLong.distance(trd, trd), 0.0, 10.0);
    checkDistance(LatLong.distance(str, str), 0.0, 10.0);
    checkDistance(LatLong.distance(trd, str), 43000.0, 45000.0);
    checkDistance(LatLong.distance(str, trd), 43000.0, 45000.0);
  }

  private void checkDistance(final double d, final double lower, final double upper) {
    assertTrue(d <= upper && d >= lower, d + " isn't between " + lower + " and " + upper);
  }

  @Test
  public void testHasGetMetaData() {
    final LatLong latLong = new LatLong(63.0, 10.0);
    assertFalse(latLong.hasMetaData());
    final MetaData metaData = latLong.getMetaData();
    assertNotNull(metaData);
    assertFalse(latLong.hasMetaData());
    metaData.addTags("aTag");
    assertTrue(latLong.hasMetaData());
    assertSame(metaData, latLong.getMetaData());
    metaData.removeTags("aTag");
    assertFalse(latLong.hasMetaData());
  }
}
